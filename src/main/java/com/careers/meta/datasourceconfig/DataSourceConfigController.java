package com.careers.meta.datasourceconfig;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/api/datasource")
public class DataSourceConfigController {

	@Autowired
	private DataSourceConfigRepository dataSourceConfigRepository;

	@GetMapping("")
	public ResponseEntity<List<DataSourceConfig>> listAllTenants() {
		List<DataSourceConfig> allDataSourceConfigs = dataSourceConfigRepository.findAll();
		System.out.println(allDataSourceConfigs);
		if (allDataSourceConfigs.isEmpty()) {
			return new ResponseEntity<List<DataSourceConfig>>(HttpStatus.NO_CONTENT);
		}

		return new ResponseEntity<List<DataSourceConfig>>(allDataSourceConfigs, HttpStatus.OK);
	}

	@GetMapping("/{name}")
	public ResponseEntity<DataSourceConfig> getTenant(@PathVariable("name") String name) {
		DataSourceConfig dataSourceConfig = dataSourceConfigRepository.findByName(name);
		if (dataSourceConfig == null) {
			return new ResponseEntity<DataSourceConfig>(HttpStatus.NOT_FOUND);
		}

		return new ResponseEntity<DataSourceConfig>(dataSourceConfig, HttpStatus.OK);
	}
}
