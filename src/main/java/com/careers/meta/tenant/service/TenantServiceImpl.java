package com.careers.meta.tenant.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.careers.meta.tenant.domain.Tenant;
import com.careers.meta.tenant.repository.TenantRepository;

@Service
public class TenantServiceImpl implements TenantService {
	
	@Autowired
	private TenantRepository tenantRepository;
	
	@Override
	public Tenant findById(String id) {
		return tenantRepository.findOne(id);
	}

	@Override
	public List<Tenant> findAllTenants() {
		return (List<Tenant>) tenantRepository.findAll();
	}

	@Override
	public Tenant saveTenant(Tenant tenant) {
		return tenantRepository.save(tenant);
	}

	@Override
	public Boolean isTenantExist(Tenant tenant) {
		if (tenant.getId() != null) {
			Tenant existingTenant = tenantRepository.findOne(tenant.getId());
			if (existingTenant == null) {
				return false;
			} else {
				return true;
			}
		} else {
			return false;
		}
	}

	@Override
	public Boolean deleteTenant(String id) {
		Tenant updatedTenant = tenantRepository.findOne(id);
		if (updatedTenant == null) {
			return false;
		} else {
			tenantRepository.delete(updatedTenant);
			return true;
		}
	}
	
}
