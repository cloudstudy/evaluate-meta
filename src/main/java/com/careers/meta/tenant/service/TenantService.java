package com.careers.meta.tenant.service;

import java.util.List;

import com.careers.meta.tenant.domain.Tenant;

public interface TenantService {
	
	Tenant findById(String id);	
	
	List<Tenant> findAllTenants();	
	
	Tenant saveTenant(Tenant tenant);
	
	Boolean isTenantExist(Tenant tenant);
		
	Boolean deleteTenant(String id);
}
