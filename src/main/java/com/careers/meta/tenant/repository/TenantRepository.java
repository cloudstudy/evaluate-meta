package com.careers.meta.tenant.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.careers.meta.tenant.domain.Tenant;

public interface TenantRepository extends JpaRepository<Tenant, String> {

}
