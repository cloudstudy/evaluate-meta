package com.careers.meta.tenant.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.careers.meta.tenant.domain.Tenant;
import com.careers.meta.tenant.service.TenantService;

@RestController
@RequestMapping("/api/tenant")
public class TenantController {

	@Autowired
	private TenantService tenantService;

	@GetMapping("")
	public ResponseEntity<List<Tenant>> listAllTenants() {
		List<Tenant> allTenants = tenantService.findAllTenants();
		if (allTenants.isEmpty()) {
			return new ResponseEntity<List<Tenant>>(HttpStatus.NO_CONTENT);
		}

		return new ResponseEntity<List<Tenant>>(allTenants, HttpStatus.OK);
	}

	@GetMapping("/{id}")
	public ResponseEntity<Tenant> getTenant(@PathVariable("id") String id) {
		Tenant tenant = tenantService.findById(id);
		if (tenant == null) {
			return new ResponseEntity<Tenant>(HttpStatus.NOT_FOUND);
		}

		return new ResponseEntity<Tenant>(tenant, HttpStatus.OK);
	}
}
