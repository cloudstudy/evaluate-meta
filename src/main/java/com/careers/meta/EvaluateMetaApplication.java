package com.careers.meta;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EvaluateMetaApplication {

	public static void main(String[] args) {
		SpringApplication.run(EvaluateMetaApplication.class, args);
	}
}
