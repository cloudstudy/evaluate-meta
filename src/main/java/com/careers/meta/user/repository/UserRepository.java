package com.careers.meta.user.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.careers.meta.user.domain.User;

public interface UserRepository extends JpaRepository<User, Long> {

	User findByUserId(String userId);
	
	String Q_FIND_ALL_TENANT_USERS = "select u from User u join u.userAuthorities ua where ua.tenantId = :tenantId";
	String Q_FIND_TENANT_USER = "select u from User u join u.userAuthorities ua where ua.tenantId = :tenantId and u.userId = :userId";
	
	@Query(Q_FIND_ALL_TENANT_USERS)
	List<User> findAllTenantUsers(@Param("tenantId") String tenantId);
	
	@Query(Q_FIND_TENANT_USER)
	User findTenantUser(@Param("tenantId") String tenantId, @Param("userId") String userId);
}
