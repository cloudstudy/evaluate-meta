package com.careers.meta.user.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.careers.meta.user.domain.UserAuthority;

public interface UserAuthorityRepository extends JpaRepository<UserAuthority, Long> {

}
