package com.careers.meta.user.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.careers.meta.user.domain.User;
import com.careers.meta.user.repository.UserRepository;

@Service
public class UserServiceImpl implements UserService {
	
	@Autowired
	private UserRepository userRepository;
	
	@Override
	public User findByUserId(String userId) {
		return userRepository.findByUserId(userId);
	}

	@Override
	public List<User> findAllUsers() {
		return (List<User>) userRepository.findAll();
	}

	@Override
	public User saveUser(User user) {
		return userRepository.save(user);
	}

	@Override
	public Boolean isUserExist(User user) {
		if (user.getId() != null) {
			User existingUser = userRepository.findOne(user.getId());
			if (existingUser == null) {
				return false;
			} else {
				return true;
			}
		} else {
			return false;
		}
	}

	@Override
	public User updateUser(Long id, User user) {
		User updatedUser = userRepository.findOne(id);
		if (updatedUser == null) {
			return null;
		}
		updatedUser.update(user);
		userRepository.save(updatedUser);		
		return updatedUser;
	}

	@Override
	public Boolean deleteUser(Long id) {
		User updatedUser = userRepository.findOne(id);
		if (updatedUser == null) {
			return false;
		} else {
			userRepository.delete(updatedUser);
			return true;
		}
	}

}
