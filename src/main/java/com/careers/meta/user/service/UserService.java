package com.careers.meta.user.service;

import java.util.List;

import com.careers.meta.user.domain.User;

public interface UserService {
	
	User findByUserId(String id);	
	
	List<User> findAllUsers();	
	
	User saveUser(User user);
	
	Boolean isUserExist(User user);	
	
	User updateUser(Long id, User user);	
		
	Boolean deleteUser(Long id);
}
