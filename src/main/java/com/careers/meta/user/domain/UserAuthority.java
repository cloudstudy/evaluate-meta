package com.careers.meta.user.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;

@Entity
@Data
public class UserAuthority {

	@Id
	@GeneratedValue
	private Long Id;

	@ManyToOne
	@JoinColumn(name = "user_id")
	@JsonIgnore
	private User user;

	private String tenantId;

	private String role;

	public UserAuthority(User user, String tenantId, String role) {
		super();
		this.user = user;
		this.tenantId = tenantId;
		this.role = role;
	}

	public UserAuthority() {

	}
}
