package com.careers.meta.user.domain;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import lombok.Data;

@Entity
@Data
public class User {

	@Id
	@GeneratedValue
	private Long id;

	@Column(nullable = false, length = 20, unique = true)
	private String userId;

	private String password;
	private String name;
	private String email;

	@OneToMany(mappedBy = "user")
	private List<UserAuthority> userAuthorities;

	public User() {

	}

	public User(String userId, String password, String name, String email, List<UserAuthority> userAuthorities) {
		super();
		this.userId = userId;
		this.password = password;
		this.name = name;
		this.email = email;
		this.userAuthorities = userAuthorities;
	}

	public void update(User updateUser) {
		this.password = updateUser.password;
		this.name = updateUser.name;
		this.email = updateUser.email;
	}
}
